# Итоговый командный проект
Деплой серверной инфраструктуры(LEMP + WordPress + Prometheus/Grafana) на несколько серверов.

**Используемый стек:** CI/CD(GitLab) + Ansible

--------------

Variables
--------------
Ключевые переменные перечислены в inventory/group_vars и inventory/host_vars

Dependencies
------------

Тестировалось и проверялось под ubuntu 20.04 lts. 

Playbooks
----------------

Для полного развертывания можно запустить один плейбук:  

ansible-playbook play-all.yml -u ubuntu --ask-vault-pass


Play samples
------------

ansible-playbook play-deploy-ssh-keys.yml -u ubuntu --extra-vars "target=production" --ask-vault-pass  

ansible-playbook play-commontools.yml -u ubuntu --extra-vars "target=production" --ask-vault-pass  

ansible-playbook play-firewall.yml -u ubuntu --extra-vars "target=production" --ask-vault-pass  

ansible-playbook play-db.yml -u ubuntu --extra-vars "target=dbservers" --ask-vault-pass  

ansible-playbook play-webapp.yml -u ubuntu --extra-vars "target=appservers" --ask-vault-pass  

ansible-playbook play-monitoring.yml -u ubuntu --extra-vars "target=monservers" --ask-vault-pass

License
-------

MIT

Author Information
------------------

Командная работа  
